import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TempuratureModule } from './tempurature/tempurature.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [TempuratureModule, UsersModule],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {}
